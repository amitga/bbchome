import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthorsService } from '../authors.service';

/**
 * @title List with selection
 */
@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})

export class AuthorsComponent implements OnInit {
  static SumValue(): any {
    throw new Error("Method not implemented.");
  }

  /*typesOfAuthors: object[] = [{id:1,name:'Yuval Noah arri'},{id:2,name:'Ami Rubinger'},{id:3,name:'J.K Rolling'},{id:4,name:'Dvora Omer'},{id:5,name:'Netanel Eliashiv'}];
  
  name;
  id;
  */

 panelOpenState = false;
 authors:any;
 authors$:Observable<any>;
 name:string;

  //constructor(private route: ActivatedRoute) { }
  constructor(private authorsservice:AuthorsService) { }

  onSubmit(){
    this.authorsservice.addAuthors(this.name);
  }
  
  ngOnInit() {
    /*
    this.books = this.booksservice.getBooks().subscribe(
      (books) => this.books = books
    )
    */
    this.authors$ = this.authorsservice.getAuthors() }

  /*ngOnInit() {
    this.name = this.route.snapshot.params.name;
    this.id= this.route.snapshot.params.id;
  }
   */
}